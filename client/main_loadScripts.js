game.foldersWithScripts = [
	"",
	"../client/mods/"
];

game.loadScripts = function() {
	for (var i = 0; i < this.foldersWithScripts.length; i++) {
		var currentFolderContents = [];
		try {
			currentFolderContents = fs.readdirSync(
				"Game/" + this.foldersWithScripts[i]
			).filter(function(fileName) {
				if (fileName.indexOf(".js") + 1) {
					return true;
				}
				
				return false;
			});
		} catch (error) {
			alert(new ReferenceError(
				"can't load content folder " +
				this.foldersWithScripts[i] +
				
				" (" + error + ")"
			));
		}
		
		for (var j = 0; j < currentFolderContents.length; j++) {
			try {
				new Function("", fs.readFileSync(
					"Game/" + 
					this.foldersWithScripts[i] +
					currentFolderContents[j],
				"utf-8"))();
			} catch (error) {
				alert(new ReferenceError(
					"can't load script " +
					this.foldersWithScripts[i] +
					currentFolderContents[j] +
					
					" (" + error + ")"
				));
			}
		}
	}
	
	for (var i in game.textures) {
		game.textures[i] = i;
	}
	for (var i in game.sounds) {
		game.sounds[i].sound = i;
	}
};
